README FOR MUON LAB USING CC-USB CRATE CONTROLLER
WRITTEN BY YAKOV KULINICH SPRING 2016
MODIFIED BY TIANSU ZHANG 2019
------------------------------------------------------
------------------------------------------------------
Simple Installation Instructions

1)  First, you need root loaded on this machine, type
    module load root/6.02.08

    Note: root might be upgraded and versions change
    Best bet is to type
    module load root/6[TAB]
    Do not type [TAB] but instead autocomplete root/6*.**.**

2)  install
    source setup.sh

3)  load environment variable XXUSBDIR
    echo env.sh (from run directory)

4)  Run directory is in MyDaq/run
    cd MyDaq/run

------------------------------------------------------
------------------------------------------------------
General Installation Instructions
For users with Linux Experience

1)  Copy xxusb_3.22 to some local directory
    I.E. ~/soft

    (You can put this anywhere, but for the sake of the instructions
    I will continue assuming this directory.)

2)  Go to ~/soft/xxusb_3.22/src

    make

    make install

Now, xxusb libraries are installed. Next, you need to add
xxusb and it's libraries to your environment variables.

3)  Using favorite editor, open ~/.bashrc

4)  At bottom, add (USERNAME is your username)
    export XXUSBDIR=/home/USERNAME/soft/xxusb_3.22

    Again, if you put the xxusb_3.22 somewhere else, change the path
    appropriately.

5)  Close terminal and open new one.
    (Alternitavely, run source ~/.bashrc)

6)  Load root every time you have a new terminal
    module load root/6.02.08

7)  Take MyDaq (from the tar package) and copy it to directory of choice

    Inside DIRECTORY_OF_CHOICE/MyDaq

    make

8)  ./runMyDaq

    A GUI will pop up. There is a text entry field.
    You can enter a name for your run (i.e. run_04_18_15)
    Otherwise, default name (output) will be used.

    Note: .root will be appended to the run name.
    	  So, in above examples, the outputs will be
    	  run_04_18_15.root or output.root

    Click Start.

    Do not exit using X at top. Finish with Stop first.

Enjoy!

------------------------------------------------------
------------------------------------------------------
TROUBLESHOOTING

1) If you get

Finding XX_USB devices and opening first one found
Found   XX_USB device
Segmentation fault

		You need to turn on and off the CAMAC crate.

2) If you get

Finding XX_USB devices and opening first one found


Device not xxusb

... Followed by a Seg Fault

    	     There is something wrong with the permissions to
	     access the usb.
	     This can be solved by going as some super user
	     and typing

	     udevadm control --reload-rules
	     udevadm trigger

	     Alternatively, contact IT support and explain to them
	     the USB udev rules are incorrect somehow.

3) If above does not work, might need to update udev rules.

	Add following lines into
	/etc/udev/rules.d
	/lib/udev/rules.d

	SUBSYSTEM=="usb", ATTR{idVendor}=="16dc", MODE=="0666"

4) If during setup you get something about usb.h not found. You probably dont have libusb-devel

	     sudo yum install libusb*
------------------------------------------------------
------------------------------------------------------
ELOG

1) Add logbook "FermilabDAQ" to elog config file before using elogd to setup server
    (config is possibly at /usr/local/elog/elogd.cfg )
    Example:

    [global]
    port = 8082
    logbook tabs = 0
    Welcome title = ZDC Prototype Logbook for 2019

    [FermilabDAQ]
    Theme = default
    Comment = ZDC DAQ Logbook for Test Beam at Fermilab
    Data dir = /home/elog/FermilabDAQ
    Attributes = Author, Subject
    Reverse sort = 1
    Required Attributes = Subject, Author
    Page Title = ZDC DAQ Logbook for Test Beam at Fermilab
    Display mode = full
    Thumbnail size = 100
2) setup server with
    elogd [-D for daemon] -c /path/to/config/elogd.cfg -v
